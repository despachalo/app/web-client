import colors from 'vuetify/es5/util/colors';
import es from 'vuetify/es5/locale/es';
import en from 'vuetify/es5/locale/en';

export default {
  srcDir: 'src',
  publicRuntimeConfig: {
    apiBaseUrl: process.env.BASE_URL || 'http://127.0.0.1:8080',
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s | Despáchalo PE',
    title: 'Bienvenido',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  plugins: [
    '~/plugins/services.js',
  ],

  components: true,

  buildModules: [
    '@nuxtjs/eslint-module',
    '@nuxtjs/vuetify',
  ],

  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/toast',
    '@nuxtjs/auth-next',
    'cookie-universal-nuxt',
  ],

  axios: {
    baseURL: process.env.BASE_URL || 'http://127.0.0.1:8080',
    proxy: false,
  },

  auth: {
    localStorage: false,
    redirect: {
      login: '/login',
      logout: '/login',
      callback: '/login',
      home: '/empresa/usuarios',
    },
    strategies: {
      local: {
        token: {
          property: 'jwt',
          global: true,
        },
        user: {
          property: false,
        },
        endpoints: {
          login: { url: '/auth/login', method: 'post' },
          logout: false,
          user: { url: '/auth/me', method: 'get' },
        },
      },
    },
  },

  pwa: {
    manifest: {
      lang: 'en',
    },
  },

  toast: {
    position: 'top-center',
    duration: 5000,
    iconPack: 'mdi',
  },

  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    lang: {
      locales: { es, en },
      current: 'es',
    },
    theme: {
      dark: false,
      themes: {
        light: {
          primary: '#11A64F',
          darken1: '#0B7437',
          secondary: '#4F4F4F',
          error: '#ED7375',
        },
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,
        },
      },
    },
  },

  build: {
  },
};
