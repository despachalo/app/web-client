import OrderService from '~/services/sync/orders';
import { READY, INCOMPLETE, PROGRAMMED } from '~/services/sync/order-status';

export const state = () => ({
  all: {
    [READY]: [],
    [INCOMPLETE]: [],
    [PROGRAMMED]: [],
  },
  orders: [],
  pageInfo: {},
  current: {
    order: {},
  },
});

export const actions = {
  async getOrders({ commit }, { search = '', page = 0, pageSize = 10, state: status = READY }) {
    const service = this.$createService(OrderService);
    try {
      const ordersPage = await service.getOrdersPage({ search, page, pageSize, state: status });
      commit('storeOrdersPage', ordersPage);
    } catch (e) {
      throw new Error('No se pudo obtener los pedidos de los clientes');
    }
  },
  async getAllOrders({ commit }, { state: status = READY }) {
    const service = this.$createService(OrderService);
    try {
      const orders = await service.getAllOrders({ state: status });
      commit('storeAllOrders', { orders, state: status });
    } catch (e) {
      throw new Error('No se pudo obtener los pedidos listos para programar');
    }
  },
};

export const mutations = {
  storeOrdersPage(_state, { data, page, pageSize, total }) {
    _state.orders = data;
    _state.pageInfo = { ..._state.pageInfo, page, pageSize, total };
  },
  storeAllOrders(_state, { orders, state: status }) {
    _state.all[status] = orders;
  },
};
