import LoadService from '~/services/sync/loads';
import { ALL } from '~/services/sync/load-status';

export const state = () => ({
  loads: [],
  pageInfo: {},
  current: {
    load: {},
  },
});

export const actions = {
  async getLoads({ commit }, { search = '', page = 0, pageSize = 10, state: status = ALL }) {
    const service = this.$createService(LoadService);
    try {
      const ordersPage = await service.getLoadsPage({ search, page, pageSize, state: status });
      commit('storeLoadsPage', ordersPage);
    } catch (e) {
      throw new Error('No se pudo obtener las cargas de datos');
    }
  },
  async loadFile(_, { file }) {
    const service = this.$createService(LoadService);
    try {
      await service.registerLoadFile({ file });
    } catch (e) {
      throw new Error('No se pudo registrar la carga de archivos a sincronizar');
    }
  },
  async getLoadDetails({ commit }, { loadId }) {
    const service = this.$createService(LoadService);
    try {
      const load = await service.getLoad({ loadId });
      commit('storeCurrentLoad', load);
    } catch (e) {
      throw new Error('No se pudo obtener los datos de la carga');
    }
  },
};

export const mutations = {
  storeLoadsPage(_state, { data, page, pageSize, total }) {
    _state.loads = data;
    _state.pageInfo = { ..._state.pageInfo, page, pageSize, total };
  },
  storeCurrentLoad(_state, load) {
    _state.current.load = load;
  },
};
