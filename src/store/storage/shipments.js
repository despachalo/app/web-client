import { filter, prop, propEq, sortBy } from 'ramda';
import ShipmentService from '~/services/storage/shipments';

export const state = () => ({
  current: {
    shipment: {},
    items: [],
    products: [],
    zones: [],
    orders: [],
  },
  shipments: [],
  pageInfo: {},
});

export const actions = {
  async getShipments({ commit }, { search = '', page = 0, pageSize = 10 }) {
    const service = this.$createService(ShipmentService);
    try {
      const shipmentsPage = await service.getShipmentsPage({ search, page, pageSize });
      commit('storeShipmentsPage', shipmentsPage);
    } catch (e) {
      console.error(e);
      throw new Error('No se pudo obtener los envíos de mercadería');
    }
  },
  async getShipmentDetails({ commit }, { shipmentId }) {
    const service = this.$createService(ShipmentService);
    try {
      const { shipment, products, checkedOrders, areas } = await service.getShipment({ shipmentId });
      commit('storeCurrentShipment', shipment);
      commit('storeCurrentProducts', products);
      commit('storeCurrentZones', areas);
      commit('storeCurrentCheckedOrders', checkedOrders);
    } catch (e) {
      console.error(e);
      throw new Error(`No se pudo obtener el envío ${shipmentId}`);
    }
  },
  async confirmItemsStorage({ commit }, { orderIds, zoneId, centerId, shipmentId }) {
    const service = this.$createService(ShipmentService);
    try {
      await service.confirmItems({ orderIds, zoneId, centerId, shipmentId });
      const { shipment, products, checkedOrders, areas } = await service.getShipment({ shipmentId });
      commit('storeCurrentShipment', shipment);
      commit('storeCurrentProducts', products);
      commit('storeCurrentZones', areas);
      commit('storeCurrentCheckedOrders', checkedOrders);
    } catch (e) {
      console.error(e);
      throw new Error('No se pudo confirmar el almacenamiento de los pallets');
    }
  },
};

export const getters = {
  itemsByProductId: _state => productId => filter(propEq('productDetailId', productId), _state.current.items),
  cantItemsByProductId: (_state, { itemsByProductId }) => productId => itemsByProductId(productId).length,
};

export const mutations = {
  storeShipmentsPage(_state, { data, page, pageSize, total }) {
    _state.shipments = data;
    _state.pageInfo = { ..._state.pageInfo, page, pageSize, total };
  },
  storeCurrentShipment(_state, { items, ...shipment }) {
    _state.current.shipment = shipment;
    _state.current.items = sortBy(prop('id'), items);
  },
  storeCurrentProducts(_state, products) {
    _state.current.products = products;
  },
  storeCurrentCheckedOrders(_state, orders) {
    _state.current.orders = orders;
  },
  storeCurrentZones(_state, areas) {
    _state.current.zones = areas;
  },
};
