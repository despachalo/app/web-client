import AreaService from '~/services/storage/areas';
import CenterService from '~/services/company/centers';

export const state = () => ({
  current: {
    area: {},
    items: [],
    products: [],
    center: {},
  },
  areas: [],
  pageInfo: {},
  availableCenters: [],
});

export const actions = {
  async getAreas({ commit }, { search = '', page = 0, pageSize = 10 }) {
    const service = this.$createService(AreaService);
    try {
      const areasPage = await service.getAreasPage({ search, page, pageSize });
      commit('storeAreasPage', areasPage);
    } catch (e) {
      console.error(e);
      throw new Error('No se pudo obtener las áreas');
    }
  },
  async getAreaDetails({ commit }, { areaId }) {
    const service = this.$createService(AreaService);
    try {
      const { details, center, products } = await service.getArea({ areaId });
      commit('storeCurrentAreaWithItems', details);
      commit('storeCurrentCenter', center);
      commit('storeCurrentProducts', products);
    } catch (e) {
      console.error(e);
      throw new Error(`No se pudo obtener el área ${areaId}`);
    }
  },
  async registerNewArea(_, { description, totalCapacity, centerId }) {
    const service = this.$createService(AreaService);
    try {
      await service.registerArea({ description, totalCapacity, centerId });
    } catch (e) {
      throw new Error('No se pudo registrar la nueva zona');
    }
  },
  async getAllCenters({ commit }) {
    const service = this.$createService(CenterService);
    try {
      const centers = await service.getAll();
      commit('storeAvailableCenters', centers);
    } catch (e) {
      throw new Error('No se pudo registrar la nueva zona');
    }
  },
};

export const mutations = {
  storeAreasPage(_state, { data, page, pageSize, total }) {
    _state.areas = data;
    _state.pageInfo = { ..._state.pageInfo, page, pageSize, total };
  },
  storeCurrentAreaWithItems(_state, { area, storedItems }) {
    _state.current.area = area;
    _state.current.items = storedItems;
  },
  storeCurrentProducts(_state, products) {
    _state.current.products = products;
  },
  storeCurrentCenter(_state, center) {
    _state.current.center = center;
  },
  storeAvailableCenters(_state, centers) {
    _state.availableCenters = centers;
  },
};
