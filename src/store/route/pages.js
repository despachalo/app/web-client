import { compose, filter, isNil, prop, propEq, reject, values } from 'ramda';
import findDescendantsAndSelf from '~/helpers/pages';

export const state = () => ({
  currentPage: null,
  pages: {
    company: {
      id: 'company',
      to: '/empresa/usuarios',
      name: 'Empresa',
      title: 'Empresa',
      icon: 'mdi-office-building',
      parent: null,
      fromRouter: false,
    },
    users: {
      id: 'users',
      to: '/empresa/usuarios',
      name: 'Usuarios',
      title: 'Usuarios',
      parent: 'company',
      fromRouter: false,
    },
    roles: {
      id: 'roles',
      to: '/empresa/roles',
      name: 'Roles',
      title: 'Roles',
      parent: 'company',
      fromRouter: false,
    },
    centers: {
      id: 'centers',
      to: '/empresa/centros',
      name: 'Centros',
      title: 'Centros de distribución',
      parent: 'company',
      fromRouter: false,
    },
    storage: {
      id: 'storage',
      to: '/almacenamiento/envios',
      name: 'Almacenamiento',
      title: 'Almacenamiento',
      icon: 'mdi-warehouse',
      parent: null,
      fromRouter: false,
    },
    shipments: {
      id: 'shipments',
      to: '/almacenamiento/envios',
      name: 'Envíos',
      title: 'Envíos',
      parent: 'storage',
      fromRouter: false,
    },
    shipment: {
      id: 'shipment',
      to: null,
      name: 'Envío',
      title: null,
      parent: 'shipments',
      fromRouter: 'id',
      showParent: true,
    },
    areas: {
      id: 'areas',
      to: '/almacenamiento/zonas',
      name: 'Zonas',
      title: 'Zonas',
      parent: 'storage',
      fromRouter: false,
    },
    newArea: {
      id: 'newArea',
      to: '/almacenamiento/zonas/nueva-area',
      name: 'Nueva zona',
      title: 'Nueva zona',
      parent: 'areas',
      fromRouter: false,
      showParent: true,
    },
    area: {
      id: 'area',
      to: '/almacenamiento/zonas',
      name: 'Zona',
      title: null,
      parent: 'areas',
      fromRouter: 'id',
      showParent: true,
    },
    center: {
      id: 'center',
      to: '/empresa/centros',
      name: 'Centro',
      title: null,
      parent: 'centers',
      fromRouter: 'id',
      showParent: true,
    },
    newCenter: {
      id: 'newCenter',
      to: '/empresa/centros/nuevo-centro',
      name: 'Nuevo centro',
      title: 'Nuevo centro',
      parent: 'centers',
      fromRouter: false,
      showParent: true,
    },
    programming: {
      id: 'programming',
      to: '/programacion/pedidos',
      name: 'Programación',
      title: 'Programación',
      icon: 'mdi-truck-check-outline',
      parent: null,
      fromRouter: false,
    },
    orders: {
      id: 'orders',
      to: '/programacion/pedidos',
      name: 'Pedidos',
      title: 'Pedidos',
      parent: 'programming',
      fromRouter: false,
    },
    dispatchs: {
      id: 'dispatchs',
      to: '/programacion/despachos',
      name: 'Despachos',
      title: 'Despachos',
      parent: 'programming',
      fromRouter: false,
    },
    programDispatch: {
      id: 'programDispatch',
      to: '/programacion/despachos/programacion-despacho',
      name: 'Programación de Despacho',
      title: 'Despacho',
      parent: 'dispatchs',
      fromRouter: false,
      showParent: true,
    },
    dispatch: {
      id: 'dispatch',
      to: '/programacion/despachos',
      name: 'Despacho',
      title: null,
      parent: 'dispatchs',
      fromRouter: 'id',
      showParent: true,
    },
    sync: {
      id: 'sync',
      to: '/sincronizacion/cargas',
      name: 'Sincronización',
      title: 'Sincronización',
      icon: 'mdi-database-sync-outline',
      parent: null,
      fromRouter: false,
    },
    loads: {
      id: 'loads',
      to: '/sincronizacion/cargas',
      name: 'Cargas',
      title: 'Cargas',
      parent: 'sync',
      fromRouter: false,
    },
    load: {
      id: 'load',
      to: '/sincronizacion/cargas',
      name: 'Carga',
      title: null,
      parent: 'loads',
      fromRouter: 'id',
      showParent: true,
    },
    newLoad: {
      id: 'newLoad',
      to: '/sincronizacion/cargas/nueva-carga',
      name: 'Nueva Carga',
      title: 'Nueva Carga',
      parent: 'loads',
      fromRouter: false,
      showParent: true,
    },
  },
});

export const actions = {
  setCurrentPage({ commit }, { currentPage }) {
    commit('setCurrentPage', currentPage);
  },
};

const isPackage = page => !page.parent;

const getSiblings = (currentPageId, pages) => {
  const currentPage = prop(currentPageId, pages);
  const currentPageParentId = prop('parent', currentPage);

  if (isNil(currentPageParentId)) {
    return [];
  }

  if (prop('showParent', currentPage)) {
    return getSiblings(currentPageParentId, pages);
  }

  const sameParent = propEq('parent', currentPageParentId);
  return compose(values, filter(sameParent))(pages);
};

export const getters = {
  getAllPackages: _state => compose(values, filter(isPackage))(_state.pages),
  getAllPages: _state => compose(values, reject(isPackage))(_state.pages),
  getCurrentPageSiblings: _state => getSiblings(_state.currentPage, _state.pages),
  getDescendantsAndSelf: page => _state => findDescendantsAndSelf(page, _state.pages),
  currentDescendantsAndSelf: _state => findDescendantsAndSelf(_state.currentPage, _state.pages),
};

export const mutations = {
  setCurrentPage(_state, currentPage) {
    _state.currentPage = currentPage;
  },
};
