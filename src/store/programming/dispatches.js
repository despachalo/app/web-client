import { join, values as ramdaValues } from 'ramda';
import { PENDING } from '~/services/programming/dispatch-status';
import DispatchService from '~/services/programming/dispatches';
import CenterService from '~/services/company/centers';

export const state = () => ({
  baseUrl: 'https://www.google.com/maps/dir',
  current: {
    dispatch: {},
    orders: [],
    vehicleDetails: [],
    center: {},
    route: {},
    storedItems: [],
  },
  dispatches: [],
  pageInfo: {},
});

export const actions = {
  async getDispatches({ commit }, { search = '', page = 0, pageSize = 10, state: status = PENDING }) {
    const service = this.$createService(DispatchService);
    try {
      const dispatchesPage = await service.getDispatchesPage({ search, page, pageSize, state: status });
      commit('storeDispatchesPage', dispatchesPage);
    } catch (e) {
      throw new Error('No se pudo obtener los despachos');
    }
  },
  async getDispatchDetails({ commit }, { dispatchId }) {
    const service = this.$createService(DispatchService);
    const centerService = this.$createService(CenterService);
    try {
      const { dispatch, neededVehicles, orders, routeRequestState, vehicleDetails, storedItems } = await service.getDispatch({ dispatchId });
      const center = await centerService.getCenter({ centerId: 1 });
      commit('storeCurrentDispatch', { ...dispatch, neededVehicles, routeRequestState });
      commit('storeCurrentOrders', orders);
      commit('storeCurrentVehicles', vehicleDetails);
      commit('storeCurrentCenter', center);
      commit('storeCurrentStoredItems', storedItems);
    } catch (e) {
      throw new Error(`No se pudo obtener el despacho ${dispatchId}`);
    }
  },
  async programNewDispatch(_, { orderIds, departureTime }) {
    const service = this.$createService(DispatchService);
    try {
      await service.programDispatch({ orderIds, departureTime });
    } catch (e) {
      throw new Error('No se pudo programar el despacho de los pedidos');
    }
  },
  async confirmCurrentDispatch(_, { dispatchId }) {
    const service = this.$createService(DispatchService);
    try {
      await service.confirmDispatch({ dispatchId });
    } catch (e) {
      throw new Error('No se pudo confirmar el despacho de los pedidos');
    }
  },
  async downloadReferralGuide({ state: { current } }, { vehicleId, orderId }) {
    const { id: dispatchId } = current.dispatch;
    const service = this.$createService(DispatchService);
    try {
      return await service.downloadReferralGuideFile({ dispatchId, vehicleId, orderId });
    } catch (e) {
      throw new Error('No se pudo descargar la guía de remisión');
    }
  },
  storeRoutes({ commit }, { vehicleId, uri }) {
    commit('storeRoute', { vehicleId, uri });
  },
  async updateVehicleAndDriver({ commit }, { dispatchId, vehicleId, data: { plate, driver } }) {
    const service = this.$createService(DispatchService);
    try {
      await service.updateVehicleAndDriverDetails({ dispatchId, vehicleId, data: { plate, driver } });
      // commit('updateVehicleAndDriver', { dispatchId, vehicleId, data: { plate, driver } });
    } catch (e) {
      throw new Error('No se pudo actualizar los datos del vehículo programado');
    }
  },
};

const getUri = (route) => {
  const values = ramdaValues(route);
  return join('', values);
};

export const getters = {
  allRouteUri: _state => getUri(_state.current.route),
};

export const mutations = {
  storeDispatchesPage(_state, { data, page, pageSize, total }) {
    _state.dispatches = data;
    _state.pageInfo = { ..._state.pageInfo, page, pageSize, total };
  },
  storeCurrentDispatch(_state, dispatch) {
    _state.current.dispatch = dispatch;
  },
  storeCurrentOrders(_state, orders) {
    _state.current.orders = orders;
  },
  storeCurrentVehicles(_state, vehicleDetails) {
    _state.current.vehicleDetails = vehicleDetails;
  },
  storeCurrentCenter(_state, center) {
    _state.current.center = center;
  },
  storeCurrentStoredItems(_state, storedItems) {
    _state.current.storedItems = storedItems;
  },
  storeRoute(_state, { vehicleId, uri }) {
    _state.current.route[vehicleId] = uri;
  },
};
