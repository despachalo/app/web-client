import stampit from 'stampit';
import Service from '~/services/service';

const RoleService = stampit.methods({
  getRolesPage({ search = '', page = 0, pageSize = 10 }) {
    return this.axios.$get('/company/roles', {
      params: {
        search,
        page,
        pageSize,
      },
    });
  },
});

export default stampit(Service, RoleService);
