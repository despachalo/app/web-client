import stampit from 'stampit';
import Service from '~/services/service';

const CenterService = stampit.methods({
  getCentersPage({ search = '', page = 0, pageSize = 10 }) {
    return this.axios.$get('/company/centers', {
      params: {
        search,
        page,
        pageSize,
      },
    });
  },
  getAll() {
    return this.axios.$get('/company/centers/all');
  },
  getCenter({ centerId }) {
    return this.axios.$get(`/company/centers/${centerId}`);
  },
  registerCenter({ name, address, location: { latitude, longitude } }) {
    return this.axios.$post('/company/centers', {
      name,
      address,
      location: {
        latitude,
        longitude,
      },
    });
  },
});

export default stampit(Service, CenterService);
