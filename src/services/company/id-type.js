
export const DNI = 'DNI';
export const PASSPORT = 'PASAPORTE';

export default [
  { text: DNI, value: DNI },
  { text: PASSPORT, value: PASSPORT },
];
