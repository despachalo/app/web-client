import stampit from 'stampit';
import Service from '~/services/service';

const UserService = stampit.methods({
  getUsersPage({ search = '', page = 0, pageSize = 10 }) {
    return this.axios.$get('/company/users', {
      params: {
        search,
        page,
        pageSize,
      },
    });
  },
});

export default stampit(Service, UserService);
