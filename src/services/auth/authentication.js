import stampit from 'stampit';
import Service from '~/services/service';

const AuthenticateService = stampit.methods({
  authenticate({ email, password }) {
    return this.axios.$post('/auth/login', { email, password });
  },
});

export default stampit(Service, AuthenticateService);
