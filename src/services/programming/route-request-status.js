
export const PENDING = 'PENDIENTE';
export const PROCESSING = 'EN PROCESO';
export const GENERATED = 'GENERADO';

export default [
  { text: PENDING, value: PENDING },
  { text: PROCESSING, value: PROCESSING },
  { text: GENERATED, value: GENERATED },
];
