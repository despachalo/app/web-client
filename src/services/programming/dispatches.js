import stampit from 'stampit';
// eslint-disable-next-line import/named
import { PENDING } from './dispatch-status';
import Service from '~/services/service';

const DispatchService = stampit.methods({
  getDispatchesPage({ search = '', page = 0, pageSize = 10, state = PENDING }) {
    return this.axios.$get('/programming/dispatches', {
      params: {
        state,
        search,
        page,
        pageSize,
      },
    });
  },
  getDispatch({ dispatchId }) {
    return this.axios.$get(`/programming/dispatches/${dispatchId}`);
  },
  programDispatch({ departureTime, orderIds }) {
    return this.axios.$post('/programming/dispatches', {
      departureTime,
      orderIds,
    });
  },
  confirmDispatch({ dispatchId }) {
    return this.axios.$post(`/programming/dispatches/${dispatchId}`);
  },
  downloadReferralGuideFile({ dispatchId, vehicleId, orderId }) {
    return this.axios.$get(`/programming/dispatches/${dispatchId}/vehicles/${vehicleId}/orders/${orderId}/guide`);
  },
  async updateVehicleAndDriverDetails({ dispatchId, vehicleId, data: { plate, driver } }) {
    await this.axios.$post(`/programming/dispatches/${dispatchId}/vehicles/${vehicleId}/details`, {
      plate,
      driver,
    });
  },
});

export default stampit(Service, DispatchService);
