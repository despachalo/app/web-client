
export const READY = 'LISTO';
export const INCOMPLETE = 'INCOMPLETO';
export const PROGRAMMED = 'PROGRAMADO';

export default [
  { text: 'LISTO PARA DESPACHO', value: READY },
  { text: 'INCOMPLETO', value: INCOMPLETE },
  { text: 'PROGRAMADO', value: PROGRAMMED },
];
