
export const PENDING = 'PENDIENTE';
export const PROCESSING = 'PROCESANDO';
export const DONE = 'PROCESADO';
export const FAILED = 'FALLIDO';
export const ALL = '';

export default [
  { text: 'TODOS', value: ALL },
  { text: PENDING, value: PENDING },
  { text: PROCESSING, value: PROCESSING },
  { text: FAILED, value: FAILED },
  { text: DONE, value: DONE },
];
