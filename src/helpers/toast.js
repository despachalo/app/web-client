
export const successOptions = {
  type: 'success',
  icon: 'mdi-check-circle-outline',
};

export const errorOptions = {
  type: 'error',
  icon: 'mdi-close-octagon-outline',
};
