
export function availableCapacityFormat(available, total) {
  return `${available} / ${total}`;
}
