import { utcToZonedTime } from 'date-fns-tz';
import { format } from 'date-fns';

export function toLocalDate(datetime, defaultValue = '') {
  if (!datetime) {
    return defaultValue;
  }

  const zonedDate = utcToZonedTime(datetime, 'America/Lima');
  return format(zonedDate, 'HH:mm a - dd/MM/yyyy');
}
