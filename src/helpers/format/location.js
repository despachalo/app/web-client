
export function locationFormat(location) {
  if (!location) {
    return '';
  }

  const { latitude, longitude } = location;
  return `Lat: ${latitude}, Long: ${longitude}`;
}
