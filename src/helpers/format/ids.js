
export function withZeros({ id, zeros = 5 }) {
  return String(id).padStart(zeros, '0');
}

export function hashId({ id, zeros = 5 }) {
  return `#${withZeros({ id, zeros })}`;
}
