import { toLocalDate } from '~/helpers/format/time';

test('Ok when format current time', () => {
  const date = new Date();
  expect(toLocalDate(date)).toBeString();
});
