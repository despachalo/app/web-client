import emailRules, { formatRequired } from '~/helpers/validations/email';

test('Ok when work with correct email format', () => {
  const emailsOk = ['jhair.guzman@gmail.com', 'jwt123@fake.com', '20163232@university.edu.pe'];
  emailsOk.forEach((email) => {
    emailRules.forEach(rule => expect(rule(email)).toBeBoolean());
  });
});

test('Show message when email format is wrong', () => {
  const emailsOk = ['prefix@', '@suffix', 'empty', null];
  emailsOk.forEach((email) => {
    expect(formatRequired(email)).toBe('El correo debe ser válido (ejemplo@despachalo.pe)');
  });
});
