import inputRequired from './required';

export const passwordRequired = inputRequired('La contraseña es requerida');

export default [passwordRequired];
