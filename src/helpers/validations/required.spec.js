import required from '~/helpers/validations/required';

test('Ok when require something and non-null is passed', () => {
  const objectIsRequired = required('Esto es requerido');
  expect(objectIsRequired(1)).toBeTruthy();
});

test('Show message when require something and null is passed', () => {
  const objectIsRequired = required('Esto es requerido');
  expect(objectIsRequired(null)).toBe('Esto es requerido');
});
