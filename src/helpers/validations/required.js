
const inputRequired = message => v => !!v || message;

export default inputRequired;
