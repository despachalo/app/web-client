import inputRequired from './required';

export const emailRequired = inputRequired('El correo es requerido');
export const formatRequired = v => /.+@.+\..+/.test(v) || 'El correo debe ser válido (ejemplo@despachalo.pe)';

export default [emailRequired, formatRequired];
