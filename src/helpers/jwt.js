import jwtDecode from 'jwt-decode';

export function decode(jwt) {
  return jwtDecode(jwt);
}
