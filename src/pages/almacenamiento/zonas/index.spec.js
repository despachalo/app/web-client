import Vue from 'vue';
import Vuetify from 'vuetify';

import { createLocalVue, mount } from '@vue/test-utils';
import ZonesPage from './index';

Vue.use(Vuetify);

describe.skip('Zones page', () => {
  const localVue = createLocalVue();
  let vuetify;

  beforeEach(() => {
    vuetify = new Vuetify();
  });

  test('Zones page', () => {
    const wrapper = mount(ZonesPage, {
      localVue,
      vuetify,
    });

    expect(wrapper.text()).toBe('');
  });
});
