import Vue from 'vue';
import Vuetify from 'vuetify';

import { createLocalVue, mount } from '@vue/test-utils';
import ZoneDetailsPage from './_id';

Vue.use(Vuetify);

describe.skip('Zone details page', () => {
  const localVue = createLocalVue();
  let vuetify;

  beforeEach(() => {
    vuetify = new Vuetify();
  });

  test('Zones details page', () => {
    const wrapper = mount(ZoneDetailsPage, {
      localVue,
      vuetify,
    });

    expect(wrapper.text()).toBe('');
  });
});
