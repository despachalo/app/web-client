import Vue from 'vue';
import Vuetify from 'vuetify';

import { createLocalVue, mount } from '@vue/test-utils';
import ShipmentsPage from './index';

Vue.use(Vuetify);

describe.skip('Shipments page', () => {
  const localVue = createLocalVue();
  let vuetify;

  beforeEach(() => {
    vuetify = new Vuetify();
  });

  test('Shipments page', () => {
    const wrapper = mount(ShipmentsPage, {
      localVue,
      vuetify,
    });

    expect(wrapper.text()).toBe('');
  });
});
