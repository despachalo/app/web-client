
export default function ({ $axios }, inject) {
  const createService = Service => Service({ axios: $axios });

  inject('createService', createService);
}
